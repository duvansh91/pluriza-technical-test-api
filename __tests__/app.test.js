const mockObjects = {
  Contents: [
    { Key: 'object_1 ' },
    { Key: 'object_2' },
    { Key: 'object_3' },
  ],
}

const mockListOBjectsPromise = {
  promise: jest.fn().mockImplementation(() => Promise.resolve(mockObjects)),
}
const mockdeleteObjectPromise = {
  promise: jest.fn().mockImplementation(() => Promise.resolve()),
}
const mockUploadPromise = {
  promise: jest.fn().mockImplementation(() => Promise.resolve()),
}

const mockListObjects = jest.fn(() => mockListOBjectsPromise)
const mockdeleteObject = jest.fn(() => mockdeleteObjectPromise)
const mockUpload = jest.fn(() => mockUploadPromise)

jest.mock('aws-sdk', () => ({
  S3: function S3() {
    this.listObjects = mockListObjects
    this.deleteObject = mockdeleteObject
    this.upload = mockUpload
  },
}))

const request = require('supertest')
const app = require('../src/app')

afterEach(() => {
  mockListObjects.mockClear()
  mockdeleteObject.mockClear()
  mockUpload.mockClear()
})

describe('POST /update', () => {
  it('Should return success', async () => {
    const response = await request(app).post('/update').send({ target_website: 'bucket' })
    expect(response.body.message).toBe('Success')
    expect(response.status).toBe(200)
    expect(mockListObjects).toHaveBeenCalledWith({ Bucket: 'bucket' })
    expect(mockdeleteObject).toHaveBeenCalledWith({
      Bucket: 'bucket',
      Key: expect.any(String),
    })
    expect(mockUpload).toHaveBeenCalledWith({
      Bucket: 'bucket',
      Key: expect.any(String),
      Body: expect.any(Buffer),
      ContentType: expect.any(String),
    })
  })
  it('Should return target_website param must be provided', async () => {
    const response = await request(app).post('/update').send()
    expect(response.body.error).toBe('target_website param must be provided')
    expect(response.status).toBe(400)
  })
  it('Should return Error uploading files', async () => {
    mockListObjects.mockReturnValueOnce(() => new Error())
    const response = await request(app).post('/update').send({ target_website: 'bucket' })
    expect(response.body.error).toBe('Error uploading files')
    expect(response.status).toBe(500)
  })
})
