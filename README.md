# Pluriza technical assessment _Part 2_

This `Node` API was created to upload a **React** `build/` folder to a specific **S3** bucket. 

*For this specific case, is used the React build/ folder contained in this repository.*

# To run this project:
```sh
npm install
```
Create a `.env` file with the `AWS` credentials:
```
AWS_ACCESS_KEY_ID=awsKey
AWS_SECRET_ACCESS_KEY=awsSecretKey
```
Then run:
```sh
npm start
```
To run the tests:
```sh
npm test
```

## Routes

- POST `/update` which receive the param `target_website` (a **S3** bucket name).

Bucket names of the 3 React apps:\
`duvan-bucket-1` for the website [plurizatest.tk](https://plurizatest.tk)\
`duvan-bucket-2` for the website [plurizatest2.tk](https://plurizatest2.tk)\
`duvan-bucket-3` for the website [plurizatest3.tk](https://plurizatest3.tk)

Success response with `status 200`:

```
{
  message: "Success"
}

```

Failed response with `status 400` if was not provided the param `target_website`:

```
{
  error: 'target_website param must be provided',
}
```

Failed response with `status 500` if was occurred an internal server error:

```
{
  error: 'Error uploading files'
}
```
