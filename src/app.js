require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser')
const routes = require('./routes')

const app = express()
const port = 4000

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use('/', routes)

app.listen(port, () => {
  // eslint-disable-next-line
  console.log(`server running on port ${port}`)
})

module.exports = app
