const path = require('path')
const fs = require('fs')
const mime = require('mime-types')
const AWS = require('aws-sdk')

const s3 = new AWS.S3({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
})

/**
 * Upload files to a S3 bucket
 * @param {string} filePath - Local folder
 * @param {string} bucket - Bucket name
 * @returns {Promise} Promise from AWS
 */
async function uploadFile(filePath, bucket) {
  const readFiles = (folder, fileList) => {
    const files = fs.readdirSync(folder) || []
    files.forEach((fileName) => {
      const root = `${folder}/${fileName}`
      if (fs.statSync(root).isDirectory()) {
        readFiles(root, fileList)
      } else {
        fileList.push(root)
      }
    })
    return fileList
  }
  const rootPath = path.resolve(__dirname, filePath)
  const fileList = readFiles(rootPath, [])
  fileList.forEach(async (fileName) => {
    await s3.upload(
      {
        Bucket: bucket,
        Key: fileName.split(`${rootPath.split('/').pop()}/`)[1],
        Body: fs.readFileSync(path.resolve(__dirname, fileName)),
        ContentType: mime.lookup(fileName),
      },
    )
      .promise()
  })
}

/**
 * Remove files from a S3 bucket
 * @param {string} bucket - Bucket name
 * @returns {Promise} Promise from AWS
 */
async function emptyBucket(bucket) {
  const response = await s3.listObjects({ Bucket: bucket }).promise()
  if (response.Contents.length > 0) {
    response.Contents.forEach(async (object) => {
      await s3.deleteObject(
        {
          Bucket: bucket,
          Key: object.Key,
        },
      )
        .promise()
    })
  }
}

module.exports = {
  uploadFile,
  emptyBucket,
}
