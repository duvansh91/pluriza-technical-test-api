const router = require('express').Router()
const update = require('./update')

router.use('/update', update)

router.get('/', (req, res) => {
  res.status(200).json({ message: 'Server running' })
})

module.exports = router
