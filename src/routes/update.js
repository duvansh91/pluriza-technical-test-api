const router = require('express').Router()
const { uploadFile, emptyBucket } = require('../services/s3')

router.post('/', async (req, res) => {
  const params = req.body
  if (!params.target_website) {
    return res.status(400).send({ error: 'target_website param must be provided' })
  }
  try {
    await emptyBucket(params.target_website)
    await uploadFile('../../build', params.target_website)
    return res.status(200).json({ message: 'Success' })
  } catch (error) {
    return res.status(500).send({ error: 'Error uploading files' })
  }
})

module.exports = router
